/*
 * Configuration for Lerntools (don't change if you really know what you'r doing!)
 */

// Supported locales - this entry **MUST** contain at least the entries from export.LOCALES in ./base/consts.js
// see https://de.wikipedia.org/wiki/Liste_der_ISO-639-1-Codes
const LOCALES = ['de', 'en', 'fr', 'hu', 'nb', 'it', 'da', 'nl']
// Default language (containig all keys)
const DEFAULT_LANG = "de"
// Location of the weblate repo locales file, relative to the position of this script
const WEBLATE_REPO = "./locales/"
// Location of lerntools base repo, relativ to the position of this script
const BASE_REPO = "./base/"
// Directories containing vue-locale files of the client repo
var LOCALE_DIRS = {
  "main": "main/locales/"
}
const BLOCK_MODULES = [
  'pairs',
  'projector',
  'tex'
]
const WEBLATE_HOST = "https://translate.codeberg.org"
const WEBLATE_PROJECT = "lerntools"
const WEBHOOK_ENABLED = true

/*
 * Don't change anything from here
 */

// Imports
const fs = require('fs')
const path = require('path')
const axios = require('axios')
const readlineSync = require('readline-sync');
const cp = require('child_process');
const glob = require('glob');
var components = []
var moduleList = []

function prepareSource() {
  const moduleSource = path.join(BASE_REPO, 'modules')
  const tmpModuleList = fs.readdirSync(moduleSource, { withFileTypes: true }).filter(dirent => dirent.isDirectory()).map(dirent => dirent.name);
  for(var i = 0; i < tmpModuleList.length; i++) {
    if (BLOCK_MODULES.indexOf(tmpModuleList[i]) == -1) {
      moduleList.push(tmpModuleList[i])
      const modLocalesPath = path.join('modules', tmpModuleList[i], 'locales')
      const absModLocalesPath = path.join(BASE_REPO, modLocalesPath)
      if (fs.existsSync(absModLocalesPath) && fs.lstatSync(absModLocalesPath).isDirectory()) {
        LOCALE_DIRS[tmpModuleList[i]] = modLocalesPath
      } else {
          console.info("Module " + tmpModuleList[i] + " does not have locales dir: ", absModLocalesPath)
      }
    }
  }
  console.debug("Taking care of following modules: ", moduleList)
}

async function checkPushRequired() {
  console.debug("Check status of weblate repository")
  const url = WEBLATE_HOST + '/api/projects/' + WEBLATE_PROJECT + '/repository/'
  const reqConfig = {headers: {
    'Authorization': 'Token ' + process.env.WEBLATE_API_TOKEN
  },
  'responseType': 'json',
  'synchronous': true}
  await axios.get(url, reqConfig).then(async function(resp) {
    if (resp.status == 200) {
      if (resp.data.needs_push) {
        console.info("Force push of translations from weblate.")
        await axios.post(url, {'operation': 'push'}, reqConfig)
      }
    }
  })
}

async function getComponents() {
  console.debug("Get weblate project components")
  const url = WEBLATE_HOST + '/api/projects/' + WEBLATE_PROJECT + '/components/'
  const reqConfig = {headers: {
    'Authorization': 'Token ' + process.env.WEBLATE_API_TOKEN
  },
  'responseType': 'json',
  'synchronous': true}
  await axios.get(url, reqConfig).then(function(resp) {
    if (resp.status == 200) {
      components = resp.data.results
    }
  })
}

async function setLockStatus(lock = false) {
  console.debug("Set lock status to: ", lock)
  const reqConfig = {headers: {
    'Authorization': 'Token ' + process.env.WEBLATE_API_TOKEN
  },
  'responseType': 'json',
  'synchronous': lock}
  for(const comp of components) {
    await axios.post(comp.lock_url, {lock: lock}, reqConfig).then((resp) => {
      if (resp.status == 200) {
        console.info("Set lock status of " + comp.name + " to ", lock)
      } else {
        console.warn("Failed setting lock status of " + comp.name + " to ", lock)
      }
    })
  }
}

function pullPath(rpath) {
  try {
    cp.execSync(
      'pushd ' + rpath + ' > /dev/null; git pull; popd',
      { stdio: 'inherit', shell: 'bash' }
    )
  } catch (err) {
    console.error("Could not pull " + rpath + " due to error.");
    throw err;
  }
  console.log("----\n");
  
  return true;
}

// check for repositories and pull
function pullRepositories() {
  // pull weblate repo
  if (!fs.existsSync(WEBLATE_REPO) || !fs.lstatSync(WEBLATE_REPO).isDirectory()) {
    console.error('Error weblate directory does not exist.')
    process.exit(1)
  }
  console.info("Pulling from Weblate-Repo: " + WEBLATE_REPO)
  pullPath(WEBLATE_REPO)
  // pull client repo
  if (!fs.existsSync(BASE_REPO) || !fs.lstatSync(BASE_REPO).isDirectory()) {
    console.error('Error client directory does not exist.')
    process.exit(1)
  }
  console.info("Pulling Base repository: " + BASE_REPO)
  pullPath(BASE_REPO)
  
  moduleList.forEach((module) => {
    const modPath = path.join(BASE_REPO, 'modules', module)
    console.info("Pulling module repository: " + modPath)
    pullPath(modPath)
  })
}

function commitPath(rpath) {
  try {
    cp.execSync(
      'pushd ' + rpath + ' > /dev/null; echo "Path: $(pwd)"; git add -A; git commit --dry-run --porcelain -q; if [ "$?" -eq "0" ]; \
      then git commit --no-status -m "Update locales" && git push; else echo "Nothing to commit"; fi; popd > /dev/null',
      { stdio: 'inherit', shell: 'bash' }
    )
  } catch (err) {
    console.error("Could not push " + rpath + " due to error.");
    return false;
  }
  console.log("----\n");
  
  return true;
}

// push changes to repositories
function pushRepositories() {
  // weblate repo
  console.info("Push changes of Weblate")
  commitPath(WEBLATE_REPO);
  console.info("Push changes of base repository")
  commitPath(BASE_REPO);
  
  moduleList.forEach((module) => {
    const modPath = path.join(BASE_REPO, 'modules', module)
    console.info("Push changes of module: " + modPath)
    commitPath(modPath);
  })
}

// main sync function
function doSync() {
  // loop through modules of application (main, etc.)
  for (const [moduleId, moduleDir] of Object.entries(LOCALE_DIRS)) {
    let weblates = {}

    // 1. Read all weblate files and create if missing
    for (const lang of LOCALES) {
      let dir = path.join(WEBLATE_REPO, moduleId)
      try {
        // first check if directory already exists
        if (!fs.existsSync(dir)) {
          fs.mkdirSync(dir);
          console.log("Directory " + dir + " is created.");
        }
      } catch (err) {
        console.log(err);
      }
      let tmpName = path.join(dir, lang + '.json')
      let tmp = {}
      try {
        // create new weblate file, if missing
        if (!fs.existsSync(tmpName)) {
          logWeblate('Create file', moduleId, lang)
          fs.writeFileSync(tmpName, JSON.stringify(tmp));
        }
        // read data from weblate file
        tmp = JSON.parse(fs.readFileSync(tmpName));
      }
      catch (e) {
        console.error('Error: Unable to read weblate file: ' + tmpName + '. Please check if the file and contains valid json.')
        console.error(e.message)
        process.exit(1)
      }
      weblates[lang] = tmp
    }

    // 2. Read all locale files
    let locales = {}
    let localeFileList = []
    try {
      localeFileList = fs.readdirSync(path.join(BASE_REPO, moduleDir))
    } catch (e) {
      console.error('Error during processing ' + moduleDir + ': ' + e.message)
    }
    // Exit if the list is empty - this is very likely an error
    if (!localeFileList || localeFileList.length === 0) {
      console.error('Error: Can\'t read json file from folder ' + moduleDir)
      process.exit(1)
    }
    // Read all json files and exit on error
    for (let file of localeFileList) {
      if (file.endsWith('json')) {
        try {
          let componentName = file.substring(0, file.indexOf('.json'))
          locales[componentName] = JSON.parse(fs.readFileSync(path.join(BASE_REPO, moduleDir, file)))
        }
        catch (e) {
          console.error('Error: Unable to read locale file: ' + file + '. Please check if the file and contains valid json.')
          console.error(e.message)
          process.exit(1)
        }
      }
    }

    // 3. Check consistency of locale files
    for (const lang of LOCALES) {
      for (const component of Object.keys(locales)) {
        // Add language to locale if missing
        if (!locales[component][lang]) {
          locales[component][lang] = {}
          logLocale('Add lang', moduleId, lang, component)
        }
        for (const key of Object.keys(locales[component][lang])) {
          // Remove keys withing locales if missing in DEFAULT_LANG
          if (!locales[component][DEFAULT_LANG].hasOwnProperty(key)) {
            delete locales[component][lang][key]
            logLocale("Delete key ", moduleId, lang, component, key)
          }
        }
      }
    }

    // 4. Add missing components and keys to weblate
    for (const lang of LOCALES) {
      for (const component of Object.keys(locales)) {
        // add missing components to weblate
        if (!weblates[lang][component]) {
          weblates[lang][component] = {}
          logWeblate('Add component', moduleId, lang, component)
        }
        // add missing keys to weblate
        if (locales[component][lang]) {
          for (const key of Object.keys(locales[component][lang])) {
            if (locales[component][lang][key] && !weblates[lang][component][key]) {
              weblates[lang][component][key] = locales[component][lang][key]
              logWeblate('Add key', moduleId, lang, component, key)
            }
          }
        }
      }
    }

    // 5. Remove unused components from weblate
    for (const lang of LOCALES) {
      for (const component of Object.keys(weblates[lang])) {
        if (!locales.hasOwnProperty(component)) {
          delete weblates[lang][component]
          logWeblate("Delete component ", moduleId, lang, component)
        }
      }
    }

    // 5.5. Copy templates
    let baseModDir = path.join(BASE_REPO, moduleDir, '..')
    let baseWeblateDir = path.join(WEBLATE_REPO, moduleDir, '..')
    let fileList = [];
    glob.sync(path.join(baseModDir, "templates", "*", "de", "*.md")).forEach((value) => {
      fileList.push(value);
      let weblateValue = value.replace(baseModDir, baseWeblateDir);
      var localeDir = path.dirname(weblateValue);
      fs.mkdirSync(localeDir, {recursive: true});
      fs.copyFileSync(value, weblateValue);
      // Do this for all languages if necessary.
      var pathArr = weblateValue.toString().split(path.sep);
      for (const lang of LOCALES) {
        let langFileArr = pathArr;
        langFileArr[langFileArr.length-2] = lang;
        const langFile = langFileArr.join(path.sep);
        var langLocDir = path.dirname(langFile);
        fs.mkdirSync(langLocDir, {recursive: true});
        if (!fs.existsSync(langFile)) {
          fs.copyFileSync(value, langFile);
        }
      }
    })

    // 5.6. Drop templates which are not required anymore.
    glob.sync(path.join(baseWeblateDir, "templates", "*", "de", "*.md")).forEach((value) => {
      let baseValue = value.replace(baseWeblateDir, baseModDir);
      if (fileList.indexOf(baseValue) == -1) {
        console.log("Remove template file " + value);
        fs.unlinkSync(value);
      }
    })

    // 5.7. Copy other languages back to main.
    glob.sync(path.join(baseWeblateDir, "templates", "*", "*", "*.md")).forEach((value) => {
      let baseValue = value.replace(baseWeblateDir, baseModDir);
      var baseDir = path.dirname(baseValue);
      // Directory must exist already!
      if (!fs.existsSync(baseDir)) {
        fs.mkdirSync(baseDir, {recursive: true});
        //console.error(baseDir + " does not exist for template " + value);
      }
      if (fileList.indexOf(baseValue) != -1) {
        // Skip german file!
      } else {
        fs.copyFileSync(value, baseValue);
      }
    })

    // 5.8. Copy texts
    let fileTextList = [];
    console.log("FOLDER: " + baseModDir);
    glob.sync(path.join(baseModDir, "text", "de", "*.md")).forEach((value) => {
      fileTextList.push(value);
      let weblateValue = value.replace(baseModDir, baseWeblateDir);
      var pathArr = weblateValue.toString().split(path.sep);
      if (pathArr[1] == "modules") {
        pathArr.splice(1, 1);
        weblateValue = pathArr.join(path.sep);
      }
      var localeDir = path.dirname(weblateValue);
      fs.mkdirSync(localeDir, {recursive: true});
      fs.copyFileSync(value, weblateValue);
      // Do this for all languages if necessary.
      for (const lang of LOCALES) {
        let langFileArr = pathArr;
        langFileArr[langFileArr.length-2] = lang;
        const langFile = langFileArr.join(path.sep);
        var langLocDir = path.dirname(langFile);
        fs.mkdirSync(langLocDir, {recursive: true});
        if (!fs.existsSync(langFile)) {
          fs.copyFileSync(value, langFile);
        }
      }
    })

    // 5.9. Drop texts which are not required anymore.
    glob.sync(path.join(baseWeblateDir, "text", "de", "*.md")).forEach((value) => {
      let baseValue = value.replace(baseWeblateDir, baseModDir);
      if (fileTextList.indexOf(baseValue) == -1) {
        console.log("Remove text file " + value);
        fs.unlinkSync(value);
      }
      // TODO: take care of other languages!
    })

    // 5.10. Copy other languages back to base.
    let textWeblatePath = baseWeblateDir;
    let textWeblateArr = baseWeblateDir.toString().split(path.sep);
    if (textWeblateArr[1] == "modules") {
      textWeblateArr.splice(1, 1);
      textWeblatePath = textWeblateArr.join(path.sep);
    }
    glob.sync(path.join(textWeblatePath, "text", "*", "*.md")).forEach((value) => {
      let baseValue = value.replace(textWeblatePath, baseModDir);
      var baseDir = path.dirname(baseValue);
      if (fileTextList.indexOf(baseValue) == -1) {
        // Directory must exist already!
        if (!fs.existsSync(baseDir)) {
          console.info("Create folder " + baseDir + " for copy " + value);
          fs.mkdirSync(baseDir, {recursive: true});
        } else {
          fs.copyFileSync(value, baseValue);
        }
      }
    })

    // 6. Copy changes from weblate to locale files
    for (const lang of LOCALES) {
      for (const component of Object.keys(locales)) {
        for (const key of Object.keys(weblates[lang][component])) {
          // Remove from weblate when removed from DEFAULT_LANG
          if (!locales[component][DEFAULT_LANG].hasOwnProperty(key)) {
            delete weblates[lang][component][key]
            logWeblate("Delete key ", moduleId, lang, component, key)
          }
          // Update in locale if changed
          if (weblates[lang][component][key] !== '' && weblates[lang][component][key] !== locales[component][lang][key]) {
            let newValue = weblates[lang][component][key]
            // Ask user for action if locale key already exists
            if (locales[component][lang][key] !== '' && locales[component][lang][key] !== undefined) {
              logLocale("Conflict ", moduleId, lang, component, key)
              console.log('(1) ' + weblates[lang][component][key] + ' (DEFAULT, from weblate)')
              console.log('(2) ' + locales[component][lang][key] + ' (from client repo)')
              var answer = ''
              if (process.env.SYNC_AUTO_ANSWER) {
                answer = process.env.SYNC_AUTO_ANSWER
              } else {
                answer = askQuestion('Please Choose string: ')
              }
              if (answer === '2') newValue = locales[component][lang][key]
              if (answer !== '1' && answer !== '2') {
                throw 'Invalid answer given. Answer not supported!'
              }
            }
            // Use weblate value if locales value is empty
            else {
              logLocale("Update key ", moduleId, lang, component, key)
            }
            // set new value
            locales[component][lang][key] = newValue
            weblates[lang][component][key] = newValue
          }
        }
      }
    }

    // 7. Save weblate files
    for (const lang of LOCALES) {
      let tmpName = path.join(WEBLATE_REPO, moduleId, lang + '.json')
      fs.writeFileSync(tmpName, JSON.stringify(weblates[lang], null, 4))
    }

    // 8. Save locale files
    for (const lang of LOCALES) {
      for (const component of Object.keys(locales)) {
        let tmpName = path.join(BASE_REPO, moduleDir, component + '.json')
        fs.writeFileSync(tmpName, JSON.stringify(locales[component], null, 4))
      }
    }
  }
}

// internal log function for changes to weblate files
function logWeblate(msg, moduleId, lang, component, key) {
  let log = 'Weblate: ' + msg + ' ' + moduleId + ' → ' + lang
  if (component) log += ' → ' + component
  if (key) log += ' → ' + key
  console.log(log)
}

// internal log function for changes to client/locale files
function logLocale(msg, moduleId, lang, component, key) {
  let log = 'Locales: ' + msg + ' ' + moduleId + ' → ' + component
  if (lang) log += ' → ' + lang
  if (key) log += ' → ' + key
  console.log(log)
}

// read user input
function askQuestion(query) {
  const answer = readlineSync.question(query);
  return answer
}

// run
async function main() {
  prepareSource()

  process.on('uncaughtException', (err) => { console.error(err && err.stack || err) });
  process.on('unhandledRejection', (reason, promise) => { console.error('Unhandled Rejection at: ', promise, ', reason: ', reason) });
  process.on('multipleResolves', (type, promise, reason) => { console.error(type, promise, reason) });
  
  // Automatic process
  if (process.env.WEBLATE_API_TOKEN) {
    console.log("1. Check and push from weblate + lock translations.")
    await getComponents()
    await checkPushRequired()
    await setLockStatus(true)
  } else if (askQuestion('1. Please push all changes from weblate, put weblate into maintainance mode and enter "Ye$": ') !== 'Ye$') {
    process.exit(1)
  }

  console.log("---\n2. Pulling client and weblate repository")
  pullRepositories()
  console.log("---\n3. Syncing between weblate and client locales")
  doSync()
  if (process.env.PUSH_AUTO) {
    console.log("---\n4. Pushing changes to weblate and client locales\n")
    pushRepositories()
  }
  else {
    askQuestion("---\n4. Please push all changes in weblate and client repos manually and press [ENTER]")
  }
  if (!WEBHOOK_ENABLED) {
    askQuestion('---\n5. Please perform pull in weblate and press [Enter]')
  }

  // Unlock translation
  console.log("\n")
  if (process.env.WEBLATE_API_TOKEN) {
    setLockStatus(false)
  }
}

main()
