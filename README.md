# Lerntools translation via Weblate for base and moduls

## Motivation

The internationalization of Lerntools is implemented in a component-oriented manner. This means that every .vue file has a `<i18n>` tag included at the end, which imports a JSON file from the `locales` folder.

Each of these files contains *all* languages for *exactly one* component, e.g. `base/main/locales/ABCDList.json`:

~~~json
{
  "de": {
    "title": "Titel",
    "name": "Name",
    "topic": "Thema"
    
  },
  "en": {
    "title": "Titel",
    "name": "Name",
    "topic": "Topic"
  }
}
~~~

As the example shows, the language split occurs at the top level of the JSON file.

Unfortunately weblate only supports language files for *exactly one* language. This repository serves as a bridge between the "two worlds". The JSON files for weblate contain *all* components for only *one* language per file, e.g. `locales/main/de.json`:

~~~json
{
    "ABCDList": {
        "title": "Titel",
        "name": "Name",
        "topic": "Thema"
    }
}
~~~

## script

The script `sync.js` serves to automate the change between the two formats. It is started directly from the root directory of this repository. Lerntools with all it submodules must be cloned into `base/` folder. The modules are directly cloned into `base/modules/`.

### Installation

~~~
npm install
~~~

### Start

~~~
node sync.js
~~~

The synchronization takes place in both directions, i.e. new translations are taken from weblate and missing keys from the locale files are provided for translation in weblate.

**Important:** Before running the script, all changes in Weblate *must* be written into the repo itself, immediately afterwards Weblate *must* read the repo again.

## Synchronization flow in detail

At the top level, the script loops through all modules of the application, currently these are `main` and `mint`. The following steps are carried out for each of these modules:

1. Read all existing weblate files; missing ones are created automatically (if, for example, a new language has been added)

2. Read all existing locale files from client repository

3. Ensure consistency of locale files; missing languages are added, keys that are no longer relevant (which were deleted in the DEFAULT_LANG) are removed

4. Add missing components and keys to weblate files; Contents are automatically taken from the locale files (so that the developer can enter these **initial** there without using the weblate repository)

5. Remove superfluous components from the weblate files, e.g. if a component no longer exists

6. Apply changes from weblate files; if a value already exists in the locale files for the specific key, the user decides manually which value to adopt.

7. Save changes to weblate files

8. Save changes to locale files


## Options

Following environment variables can be set:

 - `SYNC_AUTO_ANSWER` accepts '1' = take translations from weblate, '2' = take translations from upstream repository of lerntools. Used in case of translation conflicts.
 - `WEBLATE_API_TOKEN` contains the weblate api token which automates locking of translations, forcing push.

## Adding new languages

Follow the guide at [base repository](https://codeberg.org/lerntools/base/wiki/Add-languages)
