# Brugeradministration

Hinweise für die Administration{.subtitle}


## Nachricht an alle Benutzer

Mittels dieses Dialogs können Nachrichten an alle registrierten Benutzer senden, wobei jeweils die hinterlegte E-Mail Adresse verwendet wird. Diese Funktion sollte nur mit Bedacht genutzt werden:

- Registrierte Benutzer haben keine Möglichkeit sich von diesen Informationen „auszuschreiben“ und unnötige Nachrichten nerven...
- Bei einer großen Anzahl an Benutzern besteht das Risiko, dass die Nachrichten von anderen Systemen als Spam interpretiert werden. Damit kämen keinerlei Mails von diesem Server mehr an, Registrierung und Passwort-Reset wären damit auch nicht mehr möglich.

## Benutzerliste

Die Liste zeigt alle registrierten Benutzer des Systems mit folgenden Details:

- Persönlicher Name (hat systemintern keine weitere Bedeutung)
- Benutzername (eindeutige Benutzerkennung)
- Letztes Login (Datum der letzten Anmeldung)
- Aktiv (Administratoren können Benutzer explizit sperren)
- Rollen (Liste der zugewiesenen Rollen, diese entsprechen Modulen)

Mittels eines Klicks auf den Spaltenkopf (z.B. "Benutzername") können die Einträge aufsteigend sortiert werden, zusätzlich ist es möglich mittels eines Suchausdrucks die Auswahl einzuschränken.

Mittels der Icons in der Spalte Aktion können die Eigenschaften von Benutzern verändert werden. Es ist aus Sicherheitsgründen allerdings nicht möglich, den eigenen Benutzer zu bearbeiten. Sollte dies nötig sein, kann grundsätzlich ein zweiter Benutzer mit der Rolle admin angelegt werden und der erste damit bearbeitet werden.
