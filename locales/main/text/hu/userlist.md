# Felhasználói adminisztráció

Megjegyzések az adminisztrációhoz{.subtitle}


## Üzenet minden felhasználónak

Ez a párbeszéd lehetővé teszi, hogy üzenetet küldjön az összes regisztrált felhasználónak, az egyes esetekben tárolt e-mail cím használatával. Ezt a funkciót csak óvatosan szabad használni:

- A regisztrált felhasználók nem tudnak "lemondani" ezekről az információkról, és a felesleges üzenetek bosszantóak....
- Nagyszámú felhasználó esetén fennáll a veszélye annak, hogy az üzeneteket más rendszerek spamként értelmezik. Ez azt jelentené, hogy erről a szerverről nem érkeznének többé levelek, és a regisztráció és a jelszó visszaállítása sem lenne többé lehetséges.

## Felhasználói lista

A lista a rendszer összes regisztrált felhasználóját mutatja a következő adatokkal:

- Személynév (a rendszeren belül nincs további jelentése)
- Felhasználónév (egyedi felhasználói azonosító)
- Utolsó bejelentkezés (az utolsó bejelentkezés dátuma)
- Aktív (a rendszergazdák kifejezetten blokkolhatják a felhasználókat)
- Szerepek (a hozzárendelt szerepek listája, ezek megfelelnek a moduloknak)

Az oszlopfejlécre (pl. "felhasználónév") kattintva a bejegyzések növekvő sorrendbe rendezhetők, és lehetőség van a kiválasztás szűkítésére is egy keresőkifejezéssel.

A Művelet oszlopban található ikonok segítségével a felhasználók tulajdonságai módosíthatók. Biztonsági okokból azonban nem lehetséges a saját felhasználó szerkesztése. Ha erre szükség van, létrehozhat egy második felhasználót admin szerepkörrel, és szerkesztheti az elsőt.
