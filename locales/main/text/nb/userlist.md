# Brukerhåndtering

Merknader for administrasjonen{.subtitle}


## Melding til alle brukere

Mittels dieses Dialogs können Sie eine Nachricht an alle registrierten Benutzer senden, wobei jeweils die hinterlegte E-Mail Adresse verwendet wird. Diese Funktion sollte nur mit Bedacht genutzt werden:

- Registrerte brukere kan ikke reservere seg fra denne infoen, og unødvendige meldinger er plagsomme …
- Med stort antall brukere er det en risiko for at meldinger vil bli antatt for å være søppelpost av andre systemer. Dette betyr at ingen flere e-poster kommer fra denne tjeneren, og at registrering og passordtilbakestillinger ikke lenger er mulig.

## Brukerliste

Listen viser alle registrerte brukere for systemet, med følgende detaljer:

- Egennavn (har ingen videre anvendelse i systemet)
- Brukernavn (unik bruker-ID)
- Siste innlogging (dato for siste innlogging)
- Aktiv (administrasjon kan eksplisitt blokkere brukere)
- Roller (liste over tildelte roller, som samsvarer med moduler)

Ved å klikke på kolonneoverskriften (f.eks «brukernavn» kan oppføringer sorteres i stigende rekkefølge. Det er også mulig å begrense utvalget med et søkeuttrykk.

Egenskaper for brukere endres med ikonene i handlingskolonnen. Av sikkerhetshensyn er det ikke mulig å redigere sin egen bruker. Skulle dette være nødvendig må en ny bruker med administratorrettigheter opprettes først.
