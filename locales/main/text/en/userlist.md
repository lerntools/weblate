# User management

Notes for the administration{.subtitle}


## Message to all users

This dialogue allows you to send a message to all registered users, using the e-mail address stored. This function should only be used with caution:

- Registered users have no way of "opting out" of this information and unnecessary messages are annoying....
- With a large number of users, there is a risk that the messages will be interpreted as spam by other systems. This would mean that no more e-mails would arrive from this server, and registration and password resets would no longer be possible.

User list

The list shows all registered users of the system with the following details:

- Personal name (has no further meaning within the system)
- User name (unique user ID)
- Last login (date of last login)
- Active (administrators can explicitly block users)
- Roles (list of assigned roles, these correspond to modules)

By clicking on the column header (e.g. "user name"), the entries can be sorted in ascending order. It is also possible to limit the selection by a search expression.

User properties can be changed using the icons in the action column. For security reasons it is not possible to edit the own user. Should this be necessary, a second user with the role admin can be created and the first admin can be edited.
