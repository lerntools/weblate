% Title: Registrering af konto

En ny bruger har registreret sig på {{instanceUrl}}:

Navn: {{user.name}}
E-mail: {{user.email}}
Kommentar: {{user.comment}}
