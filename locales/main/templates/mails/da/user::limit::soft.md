% Title: Advarsel fra brugeren — blød grænse

Den indstillede bløde grænse for antallet af brugere blev nået på den 
instance [{{{config.SITE_NAME}}]]({{instanceUrl}}).
