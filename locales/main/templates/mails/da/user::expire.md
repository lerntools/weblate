% Title: Konto wird in Zukunft ablaufen

Hej {{user.name}},

Dein Konto wird in Kürze von [{{config.SITE_NAME}}]({{instanceUrl}}) wegen Inaktivität nach mehr als {{expirePeriod}} Tagen gelöscht.

Wenn Du das nicht möchtest, melde dich bitte auf der Seite [{{config.SITE_NAME}}]({{instanceUrl}}) an.

Dit team for læringsværktøjer
