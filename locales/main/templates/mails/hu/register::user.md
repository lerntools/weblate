% Title: Számlaregisztráció

Hello {{user.login}},

Ön regisztrált egy fiókot a {{instanceUrl}} oldalon.

Kérjük, kattintson az alábbi linkre a folyamat befejezéséhez:
[{{confirmUrl}}]({{confirmUrl}})

Ha nem Ön indította ezt a kérést, kérjük, hagyja figyelmen kívül ezt az e-mailt.

Az Ön Learning Tools csapata
