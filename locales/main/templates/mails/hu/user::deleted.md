% Title: Számla törölve

Hello {{user.name}},

Az Ön fiókja az összes felhasználói adattal együtt törlésre került a [{{config.SITE_NAME}}]({{instanceUrl}})-ról.

Ezt a folyamatot nem lehet visszafordítani. Új fiókot bármikor létrehozhat.

Az Ön Learning Tools csapata