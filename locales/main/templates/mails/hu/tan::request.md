% Title: Tranzakciós kód

Hello {{user.name}},

a műveletet megerősítő kód: {{tan}}.

Ha nem Ön indította ezt a kérést, kérjük, hagyja figyelmen kívül ezt az e-mailt.

Az Ön Learning Tools csapata
