% Title: A számla a jövőben megszűnik

Hello {{user.name}},

A fiókodat hamarosan törli a [{{config.SITE_NAME}}]({{instanceUrl}}) inaktivitás miatt, miután több mint {{expirePeriod}} napok.

Ha ezt nem szeretné, kérjük, jelentkezzen be a [{{config.SITE_NAME}}]({{instanceUrl}}) oldalon.

Az Ön Learning Tools csapata
