% Title: Számla törölve

Egy fiókot töröltek a [{{config.SITE_NAME}}]({{instanceUrl}}) inaktivitása miatt:

Név: {{user.name}}
E-mail: {{user.email}}
Utolsó bejelentkezés: {{user.lastLogin}}
