% Title: Jelszó visszaállítása

Hello {{user.name}},

új jelszót kértek a fiókjához a [{{config.SITE_NAME}}]({{instanceUrl}}) oldalon.

Kérjük, kattintson az alábbi linkre jelszava megerősítéséhez és visszaállításához:
[{{confirmUrl}}]({{confirmUrl}})

Ha nem kért új jelszót, kérjük, hagyja figyelmen kívül ezt az e-mailt.
