% Title: Üdvözöljük a {{config.SITE_NAME}}

Hello {{user.name}},

Üdvözöljük a {{config.SITE_NAME}} oldalon. A fiókja most már készen áll.

Menj a [{{instanceUrl}}]({{instanceUrl}}) oldalra, és kezdd el!

Az Ön Learning Tools csapata