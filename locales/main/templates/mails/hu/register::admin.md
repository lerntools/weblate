% Title: Számlaregisztráció

Új felhasználó regisztrált a {{instanceUrl}} oldalon:

Név: {{felhasználó.név}}
E-mail: {{user.email}}
Megjegyzés: {{user.comment}}
