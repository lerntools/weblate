% Title: Kennwort zurücksetzen

Ciao {{user.name}},

es wurde auf [{{config.SITE_NAME}}]({{instanceUrl}}) ein neues Kennwort für Dein Konto angefordert.

Fare clic sul seguente link per confermare e reimpostare la password:
[{{confirmUrl}}]({{confirmUrl}})

Se non avete richiesto una nuova password, ignorate questa e-mail.
