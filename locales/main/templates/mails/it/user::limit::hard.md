% Title: Avvertenza per l'utente - Limite rigido

È stato raggiunto l'hard limit impostato per il numero di utenti sul sito di 
istanza [{{config.SITE_NAME}}]({{instanceUrl}}).
