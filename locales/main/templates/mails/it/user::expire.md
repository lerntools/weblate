% Title: Il conto scadrà in futuro

Ciao {{user.name}},

Il tuo account sarà cancellato a breve da [{{config.SITE_NAME}}({{instanceUrl}}) a causa dell'inattività dopo più di {{expirePeriod}}. giorni.

Se non si desidera questo, accedere alla pagina [{{config.SITE_NAME}}({{instanceUrl}}).

Il vostro team di strumenti di apprendimento
