% Title: Avviso all'utente - Limite morbido

È stato raggiunto il soft limit impostato per il numero di utenti sul sito 
istanza [{{config.SITE_NAME}}]({{instanceUrl}}).
