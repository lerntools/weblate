% Title: Registrazione del conto

Un nuovo utente si è registrato su {{instanceUrl}}:

Nome: {{utente.nome}}
E-mail: {{utente.email}}
Commento: {{user.comment}}
