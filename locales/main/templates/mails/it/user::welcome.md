% Title: Benvenuti a {{config.SITE_NAME}}

Ciao {{user.name}},

Benvenuti a {{config.SITE_NAME}}. Il vostro account è ora pronto.

Vai a [{{instanceUrl}}({{instanceUrl}}) e inizia!

Il vostro team di strumenti di apprendimento