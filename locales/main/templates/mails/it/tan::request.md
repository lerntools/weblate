% Title: Codice di transazione

Ciao {{user.name}},

il codice per confermare l'azione è: {{tan}}.

Se non avete avviato voi stessi questa richiesta, ignorate questa e-mail.

Il vostro team di strumenti di apprendimento
