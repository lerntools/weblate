% Title: Konto slettet

Hei {{user.name}},

Kontoen din og all dens brukerdata har nå blitt slettet fra [{{config.SITE_NAME}}]({{instanceUrl}}).

Dette kan ikke angres. Du kan opprette ny konto når som helst.

Ditt Learntools-lag