% Title: Brukeradvarsel — Hard grense

Den angitte harde terskelen for antall brukere er nådd på
instansen [{{config.SITE_NAME}}]({{instanceUrl}}).
