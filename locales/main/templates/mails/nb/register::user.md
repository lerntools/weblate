% Title: Kontoregistrering

Hei {{user.login}},

Du har registrert en konto på {{instanceUrl}}-siden.

Klikk følgende lenke for å fullføre prosessen:
[{{confirmUrl}}]({{confirmUrl}})

Hvis du ikke tok initiativ til denne forespørselen kan du se bort fra denne e-posten.

Ditt Learntools-lag