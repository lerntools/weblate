% Title: Kontoen vil utløpe i løpet av kort tid

Hei {{user.name}},

Kontoen din vil snart bli slettet siden den ikke har blitt brukt på [{{config.SITE_NAME}}]({{instanceUrl}}) dager, eller ikke i det hele tatt.

Hvis du ikke ønsker dette kan du logge inn på [{{config.SITE_NAME}}]({{instanceUrl}})-siden.

Ditt Learntools-lag
