% Title: Velkommen til {{config.SITE_NAME}}

Hei {{user.name}},

Velkommen til {{config.SITE_NAME}}. Kontoen din er klar.

Gå til [{{instanceUrl}}]({{instanceUrl}}) og begynn å bruke verktøyene våre!

Ditt Learntools-lag