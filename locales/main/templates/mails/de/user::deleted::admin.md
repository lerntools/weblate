% Title: Konto gelöscht

Ein Konto wurde wegen Inaktivität von [{{config.SITE_NAME}}]({{instanceUrl}}) gelöscht:

Name: {{user.name}}
E-Mail-Adresse: {{user.email}}
Letzter Login: {{user.lastLogin}}
