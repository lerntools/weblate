% Title: Transaktionscode

Hallo {{user.name}},

der Code zur Bestätigung Deiner Aktion lautet: {{tan}}.

Wenn Du diese Anfrage nicht selbst gestellt hast, ignoriere bitte diese E-Mail.

Dein Lerntools-Team
