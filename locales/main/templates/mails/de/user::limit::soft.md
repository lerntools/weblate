% Title: Nutzerwarnung - Soft-Limit

Das gesetzte Soft-Limit für die Anzahl Nutzer wurde auf der 
Instanz [{{config.SITE_NAME}}]({{instanceUrl}}) erreicht.
