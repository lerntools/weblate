% Title: Account will expire shortly

Hello {{user.name}},

Your account will be deleted shortly for either not having logged in for more than {{expirePeriod}} days or for never having logged in at all.

If you do not want this, please log in to the [{{config.SITE_NAME}}]({{instanceUrl}}) page.

Your Lerntools-Team