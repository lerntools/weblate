% Title: User Warning - Soft Limit

The set soft limit for the number of users was reached on the 
instance [{{config.SITE_NAME}}]({{instanceUrl}}).
