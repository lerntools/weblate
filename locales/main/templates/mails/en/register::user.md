% Title: Register Account

Hello {{user.login}},

You have registered an account on the {instanceUrl}} page.

Please click the following link to complete the process:
[{{confirmUrl}}]({{confirmUrl}})

If you did not initiate this request yourself, please ignore this e-mail.

Your Lerntools-Team