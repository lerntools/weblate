% Title: User Warning - Hard Limit

The set hard limit for the number of users was reached on the 
instance [{{config.SITE_NAME}}]({{instanceUrl}}).
