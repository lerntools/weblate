% Title: Account deleted

Hello {{user.name}},

Your account with all user data has now been deleted from [{{config.SITE_NAME}}]({{instanceUrl}}).

This process cannot be reversed. You can create a new account at any time.

Your Lerntools-Team