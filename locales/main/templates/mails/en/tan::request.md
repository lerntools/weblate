% Title: Transaction Code

Hello {{user.name}},

The code to confirm your action is: {{tan}}.

If you did not initiate this request yourself, please ignore this e-mail.

Your Lerntools-Team
