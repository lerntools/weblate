% Title: Kennwort zurücksetzen

Hallo {{user.name}},

es wurde auf [{{config.SITE_NAME}}]({{instanceUrl}}) ein neues Kennwort für Dein Konto angefordert.

Bitte klicke zur Bestätigung und zum Zurücksetzen des Kennworts folgenden Link:
[{{confirmUrl}}]({{confirmUrl}})

Solltest Du kein neues Kennwort angefordert haben, ignoriere bitte diese E-Mail.
