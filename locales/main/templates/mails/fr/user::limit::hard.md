% Title: Alerte utilisateur·trice - Limite dure

La limite dure définie pour le nombre d'utilisateurs·trices a été atteinte 
sur l'adresse de l'instance [{{config.SITE_NAME}}]({{instanceUrl}}).
