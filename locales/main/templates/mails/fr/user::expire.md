% Title: Compte bientôt expirant

Bonjour {{user.name}},

Ton compte sera bientôt supprimé par [{{config.SITE_NAME}}]({{instanceUrl}}) pour cause d'inactivité après plus de {{expirePeriod}} jours.

Si tu ne le souhaites pas, connecte-toi sur la page [{{config.SITE_NAME}}]({{instanceUrl}}).

Ton équipe Lerntools
