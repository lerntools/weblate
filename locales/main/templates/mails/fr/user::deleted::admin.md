% Title: Compte supprimé

Un compte a été supprimé à cause de l'inactivité de [{{config.SITE_NAME}}]({{instanceUrl}}) :

Nom : {{user.name}}
E-mail : {{user.email}}
Dernière connexion : {{user.lastLogin}}
