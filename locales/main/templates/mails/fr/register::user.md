% Title: Enregistrement du compte

Bonjour {{user.login}},

Tu as enregistré un compte sur la page {{instanceUrl}}.

Clique sur le lien suivant pour terminer le processus :
[{{confirmUrl}}]({{confirmUrl}})

Si tu n'as pas lancé cette demande toi-même, tu peux ignorer cet e-mail.

Ton équipe Lerntools
