# Ideensammlung

Anleitung für Autoren{.subtitle}

Eine „Ideensammlung“ dient dem gemeinsamen Brainstorming zu einem bestimmten Thema. Sie besteht aus einer Anzahl von Karten, die jeweils mehrere Inhalte aufnehmen können. Ein Inhalt kann ein Text, ein Bild oder auch ein Link sein.

![](text/de/ideas/example1.png){.center}

An einer Ideensammlung können *gleichzeitig* mehrere Personen arbeiten, die Änderungen werden dabei automatisch und (fast) ohne Verzögerung synchronisiert. Dazu ist allerdings ein aktueller Webbrowser (z.B. Firefox, Safari, Chrome) mit Unterstützung für Websockets nötig.

## Autor

### Erstellen einer Ideensammlung

Nur ein eine auf der Webseite registrierte Person mit der entsprechenden Berechtigung, kann als *AutorIn* eine neue Ideensammlung erstellen:

1. Anmelden an der Webseite (wichtig, sonst erscheint die Ideensammlung in der Teilnehmersicht)
2. Navigation zum Punkt Ideensammlung
3. Anklicken des blauen Plus-Symbols

### Einstellungen einer Ideensammlung

![](text/de/ideas/example2.png){.center}

1. Grundlegende Einstellungen

	- Titel: Kurze Bezeichnung der Ideensammlung, erscheint bei Teilnehmer als Titel der Seite.
	- Beschreibung: Kurze Erläuterung, erscheint als zweite Zeile im Titel.
	- Ticket: Zeichenfolge, die weiteren *Teilnehmern* den Zugriff auf die Ideensammlung ermöglicht.

2. Teilnehmer dürfen Inhalte...:

	- hinzufügen: Recht, neue Inhalte auf bestehende Karten zu schreiben.
	- ändern: Recht, bestehende Inhalte auf einer Karte zu ändern.
	- löschen: Recht, bestehende Inhalte zu löschen (auch für Verschieben nötig).

3. Teilnehmer dürfen Karten...:

	- hinzufügen: Recht, neue Karten hinzuzufügen.
	- umbenennen: Recht, den Titel (und die Farbe) einer bestehenden Karte zu ändern.
	- löschen: Recht, eine bestehende Karte zu löschen.

4. Weitere Einstellungen

	- Teilnehmer dürfen Dateien hochladen (Upload): ermöglicht den Upload von Dateien.
	- Präsentation von Karten als Slideshow: Optional können die Inhalte einer Karte als Slideshow dargestellt werden.

„Speichern“ übernimmt die Einstellungen, die neu erstellte Ideensammlung erscheint jetzt in der Übersicht.

### Übersicht der Ideensammlungen

Die Ideensammlungen werden in der Übersicht tabellarisch dargestellt:

![](text/de/ideas/example3.png){.center}

- Anzeigen (Auge): Anzeigen der Ideensammlung, wobei der Autor grundsätzlich alle Rechte besitzt.
- Bearbeiten (Stiftsymbol): Ändern der Einstellungen der Ideensammlung, z.B. kann eine abgeschlossene Sammlung anschließend komplett gesperrt werden und als reine Informationstafel verwendet werden.
- Teilen (Share): Anzeige des Tickets und eines QR-Codes für die Teilnehmer.
- Duplizieren (Quadrate): Duplizieren einer bestehenden Ideensammlung.
- Export (Pfeil): Exportieren der Sammlung im Markdown bzw. HTML-Format. Möchten Sie die Sammlung z.B. mit LibreOffice weiterbearbeiten, ist dies über das HTML-Format gut möglich.
- Löschen (Mülleimer): Löschen der Ideensammlung mit allen Karten und Inhalten. Dieser Schritt kann nicht rückgängig gemacht werden.

### Export und Import

Der Export ist in folgenden Formaten möglich:

- JSON: Maschinenlesbares Format, eignet sich zum späteren Import.
- JSON & Uploads – für den späteren Import, inklusive hochgeladener Dateien (ohne Garantie).
- HTML: Menschenlesbares Format, kann im Browser oder Textverarbeitungssystem geöffnet werden.

Der Import ist nur im JSON Format möglich. Bei JSON & Uploads _sollten_ sie im Upload enthalten sein, jedoch gibt es keine Garantie. Zur Sicherheit sollten Sie separat gesichert werden.

### Tipps

- Als Autor können einzelne Karten gesperrt werden. Dies ist nach dem Anzeigen (siehe oben) einer Ideensammlung durch einen Klick auf den Kopf der Karte möglich.
- Auch das Verstecken von Karten ist möglich. Bitte beachten Sie, dass die Daten technisch versierten Benutzern dennoch zur Verfügung stehen, also lediglich die Anzeige unterdrückt wird.
- Die Reihenfolge der Karten richtet sich grundsätzlich nach ihrer Erstellung. Allerdings kann nachträglich eine Sortierung festgelegt werden (ebenfalls durch Klick auf den Kopf einer Karte).
- Inhalte können per Drag & Drop oder über das Kontextmenü von einer Karte zur anderen verschoben werden.

##	Teilnehmer

### Beitreten zu einer Ideensammlung

Um an einer Ideensammlung teilzunehmen, stehen zwei Möglichkeiten zur Verfügung:

1. Nutzung des QR-Codes, den der Autor mit der Funktion Teilen anzeigt
2. Aufruf des Punkts Ideensammlung in der Navigation und Eingabe des Tickets

### Arbeiten mit der Ideensammlung

Die Möglichkeiten der Bearbeitung sind davon abhängig, welche Rechte der Autor definiert hat. Eine kurze Hilfe zur den möglichen Aktionen kann mit der Schaltfläche „Hilfe“ angezeigt werden.
