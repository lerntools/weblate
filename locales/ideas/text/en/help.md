# Brainstorming session

Instructions for authors{.subtitle}

A "brainstorming session" is used for brainstorming together on a specific topic. It consists of a number of cards, each of which can hold several pieces of content. The content can be a text, a picture or even a link.

![](text/en/ideas/example1.png){.center}

Several people can work on a brainstormin session *at the same time*, and the changes are synchronised automatically and (almost) without delay. However, this requires a current web browser (e.g. Firefox, Safari, Chrome) with support for websockets.

## Author

### Create a brainstorming session

Only a person registered on the website with the appropriate authorisation can create a new collection of ideas as an *author*:

1. log in to the website (important, otherwise the collection of ideas will appear in the participants' view).
2. navigate to the collection of ideas
3. click on the blue plus symbol

### Settings of a brainstorming session

![](text/en/ideas/example2.png){.center}

1. Basic settings

	- Title: Short description of the collection of ideas, appears as the title of the page for participants.
	- Description: Short explanation, appears as the second line in the title.
	- Ticket: String that allows other *participants* to access the collection of ideas.

2. Permissions of participants for contents...:

	- add: Right to write new content on existing cards.
	- change: Right to change existing content on a card.
	- delete: Right to delete existing content (also necessary for moving).

3. Permissions of participants for cards...:

	- add: Right to add new cards.
	- rename: Right to change the title (and colour) of an existing card.
	- delete: Right to delete an existing card.

4. More settings

	- Participants may upload files (upload): enables the upload of files.
	- Presentation of cards as slideshow: Optionally, the contents of a card can be presented as a slideshow.

"Save" accepts the settings, the newly created collection of ideas now appears in the overview.

### Overview of the brainstorming sessions

The brainstormin sessions are presented in tabular form in the overview:

![](text/en/ideas/example3.png){.center}

- Display (triangle): Display the collection of ideas, whereby the author basically owns all rights.
- Edit (pencil symbol): Change the settings of the brainstorming session, e.g. a completed session can then be completely locked and used as a pure information board
- Share (Share): Display the ticket and a QR code for the participants
- Duplicate (Squares): Duplicate an existing collection of ideas
- Export (arrow): Export the collection in Markdown or HTML format. If you want to edit the collection further, e.g. with LibreOffice, this is easily possible via the HTML format.
- Delete (bin): Delete the collection of ideas with all cards and contents. This step cannot be undone.

### Export and import

Export is possible in the following formats:

- JSON: Machine-readable format, suitable for later import.
- JSON & Uploads - for later import, including uploaded files (no guarantee).
- HTML: Human readable format, can be opened in browser or word processor.

The import is only possible in JSON format. For JSON & uploads, they _should_ be included in the upload, but there is no guarantee. For security, they should be saved separately.

### Hints

- As an author, individual cards can be locked. This is possible after displaying (see above) a brainstorming session by clicking on the header of the card.
- It is also possible to hide cards. Please note that the data is still available to technically skilled users as only the display is suppressed.
- The order of the cards is basically determined by their creation. However, a sorting can be defined subsequently (also by clicking on the header of a card).
- Contents can be moved from one card to another by drag & drop or via the context menu.

##	participants

### Joining a brainstorming session

There are two possibilities to join a brainstorming session:

1. use the QR code that the author displays with the share function.
2. call up the item brainstorming session in the navigation and enter the ticket

### Working with the brainstorming session

The editing options depend on the rights defined by the author. A short help on the possible actions can be displayed with the "Help" button.
