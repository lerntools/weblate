# Sessione di brainstorming

Istruzioni per gli autori{.subtitle}

Una "sessione di brainstorming" è usata per parlare tutti insieme di uno specifico argomento. Consistente in un numero di carte, ciascuna delle quali può contenere parti di contenuto. Il contenuto può essere testo, un immagine o anche un link.

![](text/it/ideas/example1.png){.center}

Molte persone possono lavorare in una sessione di brainstorming *insieme*, e i cambiamenti sono sincronizzati automaticamente e (quasi) senza ritardo. Comunque, questo richiede un web browser (e.g. Firefox, Safari, Chrome) che supporti i websocket.

## Autore

### Crea una sessione di brainstorming

Solo una persona registrata sul sito con i privilegi appropriati può creare una nuova collezione di idee come un *autore*:

1. Entra nel sito (importante, altrimenti le collezioni di idee appariranno dalla prospettiva dei partecipanti)
2. Naviga fino alla collezione di idee
3. Clicca sul simbolo blue +

### Impostazioni di una sessione di brainstorming

![](text/it/ideas/example2.png){.center}

1. Impostazioni base

	- Titolo: Descrizione veloce della collezione di idee, appare come titolo della pagina dei partecipanti.
	- Descrizione: piccola spiegazione, apparira come seconda riga del titolo.
	- Bigllietto: stringa che permettera ad altri *partecipanti* di accedere alla collezione di idee.

2. Permessi dei partecipanti per il contenuto...:

	- Aggiungi: per aggiungere nuovo contenuto alle carte gia esistenti.
	- Cambia: per cambiare il contenuto esistente delle carte.
	- Elimina: per eliminare il contenuto (necessario anche per spostare).

3. Permessi dei partecipanti per le carte...:

	- Aggiungi: per creare una nuova carta.
	- Rinomina: per cambiare titolo (e colore) a una carta gia esistente.
	- Elimina: per eliminare una carta.

4. Altre impostazioni

	- Partecipanti potrebbero caricare files (upload): abilita il caricamento di files.
	- Presenta le carte come una presentazione: opzionale, il contenuto delle carte può essere presentato come uno slideshow.

"Salva" salva le impostazioni, le collezioni di idee appena create appariranno nella panoramica.

### Panoramica delle sessioni di brainstorming

Le sessioni di brainstorming sono presentato in modo tabulato nella panoramica:

![](text/it/ideas/example3.png){.center}

- Anzeigen (Auge): Anzeigen der Ideensammlung, wobei der Autor grundsätzlich alle Rechte besitzt.
- Bearbeiten (Stiftsymbol): Ändern der Einstellungen der Ideensammlung, z.B. kann eine abgeschlossene Sammlung anschließend komplett gesperrt werden und als reine Informationstafel verwendet werden.
- Teilen (Share): Anzeige des Tickets und eines QR-Codes für die Teilnehmer.
- Duplizieren (Quadrate): Duplizieren einer bestehenden Ideensammlung.
- Export (Pfeil): Exportieren der Sammlung im Markdown bzw. HTML-Format. Möchten Sie die Sammlung z.B. mit LibreOffice weiterbearbeiten, ist dies über das HTML-Format gut möglich.
- Löschen (Mülleimer): Löschen der Ideensammlung mit allen Karten und Inhalten. Dieser Schritt kann nicht rückgängig gemacht werden.

### Export und Import

Der Export ist in folgenden Formaten möglich:

- JSON: Maschinenlesbares Format, eignet sich zum späteren Import.
- JSON & Uploads – für den späteren Import, inklusive hochgeladener Dateien (ohne Garantie).
- HTML: Menschenlesbares Format, kann im Browser oder Textverarbeitungssystem geöffnet werden.

Der Import ist nur im JSON Format möglich. Bei JSON & Uploads _sollten_ sie im Upload enthalten sein, jedoch gibt es keine Garantie. Zur Sicherheit sollten Sie separat gesichert werden.

### Tipps

- Als Autor können einzelne Karten gesperrt werden. Dies ist nach dem Anzeigen (siehe oben) einer Ideensammlung durch einen Klick auf den Kopf der Karte möglich.
- Auch das Verstecken von Karten ist möglich. Bitte beachten Sie, dass die Daten technisch versierten Benutzern dennoch zur Verfügung stehen, also lediglich die Anzeige unterdrückt wird.
- Die Reihenfolge der Karten richtet sich grundsätzlich nach ihrer Erstellung. Allerdings kann nachträglich eine Sortierung festgelegt werden (ebenfalls durch Klick auf den Kopf einer Karte).
- Inhalte können per Drag & Drop oder über das Kontextmenü von einer Karte zur anderen verschoben werden.

##	Teilnehmer

### Beitreten zu einer Ideensammlung

Um an einer Ideensammlung teilzunehmen, stehen zwei Möglichkeiten zur Verfügung:

1. Nutzung des QR-Codes, den der Autor mit der Funktion Teilen anzeigt
2. Aufruf des Punkts Ideensammlung in der Navigation und Eingabe des Tickets

### Arbeiten mit der Ideensammlung

Die Möglichkeiten der Bearbeitung sind davon abhängig, welche Rechte der Autor definiert hat. Eine kurze Hilfe zur den möglichen Aktionen kann mit der Schaltfläche „Hilfe“ angezeigt werden.
