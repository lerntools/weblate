# Indsamling af idéer

Vejledning til forfattere{.subtitle}

En "samling af idéer" bruges til at lave en brainstorming sammen om et bestemt emne. Den består af en række kort, som hver kan indeholde flere dele af indholdet. Et indhold kan være en tekst, et billede eller endda et link.

![](text/de/ideas/beispiel1.png){.center}

Flere personer kan arbejde på en samling af idéer *på samme tid*, og ændringerne synkroniseres automatisk og (næsten) uden forsinkelse. Dette kræver dog en aktuel webbrowser (f.eks. Firefox, Safari, Chrome) med understøttelse af websockets.

## Forfatter

#### Oprettelse af en samling af idéer

Kun en person, der er registreret på webstedet med den rette autorisation, kan oprette en ny idéopsamling som *forfatter*:

1. Log ind på webstedet (vigtigt, ellers vil idéopsamlingen blive vist i deltagernes visning).
2. navigere til idéopsamlingen
3. klik på det blå plus-symbol

#### Indstillinger af en samling af idéer

![](text/de/ideas/beispiel2.png){.center}

1. grundlæggende indstillinger

	- Titel: Kort beskrivelse af idéopsamlingen, der vises som sidens titel for deltagerne.
	- Beskrivelse: Kortfattet forklaring, vises som den anden linje i titlen.
	- Billet: Streng, der giver andre *deltagere* adgang til idéopsamlingen.

2. Deltagerne kan omfatte indhold....:

	- tilføje: Ret til at skrive nyt indhold på eksisterende kort.
	- ændring: Ret til at ændre eksisterende indhold på et kort.
	- slette: Ret til at slette eksisterende indhold (også nødvendigt ved flytning).

3. deltagerne kan bruge kort....:

	- tilføje: Ret til at tilføje nye kort.
	- omdøbe: Ret til at ændre titlen (og farven) på et eksisterende kort.
	- slette: Højre for at slette et eksisterende kort.

4. yderligere indstillinger

	- Deltagerne kan uploade filer (Upload): Aktiverer upload af filer.
	- Præsentation af kort som diasshow: Indholdet af et kort kan eventuelt præsenteres som et diasshow.

"Gem" accepterer indstillingerne, og den nyoprettede samling af idéer vises nu i oversigten.

#### Oversigt over idésamlinger

Idésamlingerne er præsenteret i tabelform i oversigten:

![](text/de/ideas/beispiel3.png){.center}

- Display (trekant): Viser en samling af idéer, hvor forfatteren i princippet ejer alle rettigheder.
- Rediger (blyantssymbol): Ændre indstillingerne for idésamlingen, f.eks. kan en afsluttet samling låses helt og udelukkende bruges som informationstavle.
- Aktie (Share): Visning af billetten og en QR-kode for deltagerne.
- Duplikat (firkanter): Duplikere en eksisterende samling af idéer.
- Eksport (pil): Eksporter samlingen i Markdown- eller HTML-format. Hvis du ønsker at redigere samlingen yderligere, f.eks. med LibreOffice, er det let muligt via HTML-formatet.
- Slet (bin): Slet idéopsamlingen med alle kort og indhold. Dette trin kan ikke gøres om.

#### Eksport og import

Eksporten er mulig i følgende formater:

- JSON: Maskinlæsbart format, der er egnet til senere import.
- JSON & Uploads - til senere import, herunder uploadede filer (ingen garanti).
- HTML: Menneskeligt læsbart format, kan åbnes i en browser eller i et tekstbehandlingsprogram.

Import er kun mulig i JSON-format. For JSON & uploads burde de _skal_ være inkluderet i upload, men der er ingen garanti. Af sikkerhedshensyn bør de gemmes separat.

### Tips

- Som forfatter kan du låse individuelle kort. Dette er muligt efter at have vist (se ovenfor) en samling af idéer ved at klikke på kortets overskrift.
- Det er også muligt at skjule kort. Bemærk venligst, at dataene stadig er tilgængelige for teknisk dygtige brugere, dvs. kun visningen er undertrykt.
- Rækkefølgen af kortene er grundlæggende bestemt af deres oprettelse. Der kan dog defineres en sortering efterfølgende (også ved at klikke på kortets overskrift).
- Indholdet kan flyttes fra et kort til et andet ved at trække og slippe det eller via kontekstmenuen.

## Deltagere

### Deltag i en samling af idéer

Der er to måder at deltage i en idéindsamling på:

1. Brug den QR-kode, som forfatteren viser med funktionen Del.
2. kald varen Idea collection i navigationen og indtast billetten

#### Arbejde med idéopsamling

Redigeringsmulighederne afhænger af de rettigheder, som forfatteren har defineret. En kort hjælp om de mulige handlinger kan vises med knappen "Hjælp".
