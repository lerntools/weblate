# Ötletgyűjtemény

Útmutató szerzőknek{.subtitle}

Az "ötletgyűjteményt" egy adott témával kapcsolatos közös ötletelésre használják. Több kártyából áll, amelyek mindegyike több tartalmat tartalmazhat. A tartalom lehet szöveg, kép vagy akár link is.

![](text/hu/ideas/example1.png){.center}

Több ember dolgozhat egy ötletgyűjteményen *egyszerre*, és a változtatások automatikusan és (szinte) késedelem nélkül szinkronizálódnak. Ehhez azonban szükség van egy aktuális webböngészőre (pl. Firefox, Safari, Chrome), amely támogatja a websocketeket.

## Szerző

### Ötletgyűjtemény létrehozása

Csak a weboldalon regisztrált, megfelelő jogosultsággal rendelkező személy hozhat létre új ötletgyűjteményt *szerzőként*:

1. Jelentkezzen be a weboldalra (fontos, különben az ötletgyűjtemény megjelenik a résztvevők nézetében).
2. navigáljon az ötletgyűjteményhez
3. kattintson a kék plusz szimbólumra

### Ötletek gyűjteményének beállításai

![](text/hu/ideas/example2.png){.center}

1. Alapszintű beállítások

	- Cím: Az ötletgyűjtemény rövid leírása, a résztvevők számára az oldal címeként jelenik meg.
	- Leírás: Rövid magyarázat, a cím második sorában jelenik meg.
	- Jegy: karakterlánc, amely lehetővé teszi más *résztvevők* számára, hogy hozzáférjenek az ötletgyűjteményhez.

2. A résztvevők tartalmazhatnak tartalmat....:

	- add: Új tartalom írásának joga a meglévő kártyákhoz.
	- változás: A térképen meglévő tartalom megváltoztatásának joga.
	- törölni: A meglévő tartalom törlésének joga (költözéshez is szükséges).

3. A résztvevők a kártyákat...:

	- add: Új kártyák hozzáadásának joga.
	- átnevezni: A meglévő térkép címének (és színének) megváltoztatásához való jog.
	- törölni: Meglévő térkép törlésének joga.

4. további beállítások

	- A résztvevők fájlokat tölthetnek fel (Feltöltés): lehetővé teszi a fájlok feltöltését.
	- Térképek diavetítésként történő bemutatása: A térképek tartalma diavetítésként is bemutatható.

A "Mentés" elfogadja a beállításokat, az újonnan létrehozott ötletgyűjtemény mostantól megjelenik az áttekintésben.

### Az ötletgyűjtemények áttekintése

Az ötletgyűjtemények táblázatos formában szerepelnek az áttekintésben:

![](text/hu/ideas/example3.png){.center}

- Kijelző (háromszög): Az ötletek gyűjteményének megjelenítése, ahol a szerző alapvetően minden joggal rendelkezik.
- Szerkesztés (ceruzaszimbólum): Az ötletgyűjtemény beállításainak módosítása, pl. egy elkészült gyűjteményt teljesen le lehet zárni, és tisztán információs táblaként lehet használni.
- Részesedés (Share): A jegy és egy QR-kód megjelenítése a résztvevők számára
- Duplikátum (négyzetek): Meglévő ötletgyűjtemény duplikálása
- Export (nyíl): A gyűjtemény exportálása Markdown vagy HTML formátumban. Ha tovább szeretné szerkeszteni a gyűjteményt, például a LibreOffice programmal, ez a HTML formátumon keresztül könnyen lehetséges.
- Törlés (kuka): Törölje az ötletgyűjteményt az összes térképpel és tartalommal együtt. Ezt a lépést nem lehet visszacsinálni.

### Export és import

Az exportálás a következő formátumokban lehetséges:

- JSON: Gépileg olvasható formátum, amely alkalmas későbbi importálásra.
- JSON és feltöltések - későbbi importáláshoz, beleértve a feltöltött fájlokat is (nincs garancia)
- HTML: Ember által olvasható formátum, böngészőben vagy szövegszerkesztőben megnyitható.

Az importálás csak JSON formátumban lehetséges. A JSON és a feltöltések esetében a feltöltésnek tartalmaznia kell őket, de erre nincs garancia. A biztonság kedvéért ezeket külön kell elmenteni.

### Tippek

- Szerzőként az egyes térképek zárolhatók. Ez a térkép fejlécére kattintva lehetséges, miután (lásd fentebb) egy ötletgyűjteményt megjelenítettünk.
- Lehetőség van a térképek elrejtésére is. Felhívjuk figyelmét, hogy az adatok továbbra is elérhetőek a technikailag képzett felhasználók számára, azaz csak a megjelenítés van elnyomva.
- A térképek sorrendjét alapvetően a létrehozásuk határozza meg. A rendezés azonban utólag is meghatározható (szintén a térkép fejlécére kattintva).
- A tartalmak áthelyezhetők egyik térképről a másikra drag & drop segítségével (vagy a kontextus menü segítségével).

## Résztvevők

### Csatlakozz egy ötletgyűjteményhez

Az ötletgyűjtésben való részvételnek két módja van:

1. Használja a QR-kódot, amelyet a szerző a Megosztás funkcióval jelenít meg
2. Hívja fel a navigációban az Ötletgyűjtemény elemet, és adja meg a jegyet

### Az ötletek gyűjteményével való munka

A szerkesztési lehetőségek a szerző által meghatározott jogoktól függnek. A "Súgó" gombbal rövid segítség jeleníthető meg a lehetséges műveletekről.
