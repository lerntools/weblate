# Quiz ABCD

## Instructions pour les auteur·es


Le « Quiz ABCD » est une variation de « l'avis du public » de « Qui veut gagner des millions ? » Un jeu se compose de plusieurs questions pour lesquelles il existe quatre réponses prédéfinies (A, B, C et D), dont une seule est valable. La question est présentée simultanément à tous les participant·es (sur le vidéoprojecteur), qui choisissent la réponse dans le cadre d'un « vote secret » via leur smartphone. Le temps imparti pour répondre à une question est limité. L'animateur et les participant·es n'ont pas besoin d'un logiciel spécial, un navigateur récent suffit.



## Démarrer un jeu

Seule une personne inscrite sur le site et disposant des autorisations nécessaires peut, en tant que modérateur, lancer un nouveau jeu ou même créer de nouvelles questions :

1. Se connecter au site web (important, sinon le Quiz ABCD apparaît dans la vue des participant·es)
2. Naviger vers l'élément Quiz ABCD

![](text/fr/abcd/example1.png){.center}

3. Cliquer sur l'icône de démarrage (triangle)

![](text/fr/abcd/example2.png){.center}

Toutes les collections de questions créées par toi-même ainsi que celles qui ont été marquées comme publiques par d'autres auteur·es sont disponibles. Pour simplifier la recherche, il est possible de limiter l'affichage à un sujet/une matière spécifique dans la liste de sélection.

Les collections publiques d'autres personnes ne peuvent pas être modifiées ni supprimées. C'est-à-dire que ces icônes ne sont pas affichées dans ce cas.

![](text/fr/abcd/example3.png){.center}

## Le lobby

Après le lancement du Quiz ABCD, le lobby apparaît et contient un code QR pour les participant·es.
Au lieu du code QR, il est également possible d'utiliser l'URL affichée ainsi que la clé.

![](text/fr/abcd/example4.png){.center}


Au début, toutes et tous les participant·es sont invité·es à attribuer un NickName d'au moins 3 caractères, qui ne sera pas enregistré de manière permanente dans la base de données. Ce pseudo apparaît ensuite dans la partie droite du lobby.

![](text/fr/abcd/example5.png){.center}

Les noms inappropriés peuvent être bannis du jeu via le symbole de la poubelle. Une page sobre avec quatre boutons (A, B, C, D) s'affiche alors chez les participant·es.

![](text/fr/abcd/example6.png){.center}

## Questions & réponses

Lorsque toutes et tous les participant·es sont dans le jeu, c'est-à-dire visibles dans le lobby, la personne chargée de la modération lance le quiz en cliquant sur le bouton « Démarrer ». Les questions et les réponses apparaissent en alternance. Les participant·es ne peuvent voter que si une question est active.

![](text/fr/abcd/example7.png){.center}

## Conseils d'utilisation

Il est également possible d'utiliser la barre d'espace à la place de la flèche bleue pour passer à la page suivante.

Après chaque question, la réponse correcte est affichée. 

![](text/fr/abcd/example8.png){.center}

Le score intermédiaire actuel est alors affiché pour les participants·es.
À la fin du jeu, un récapitulatif des participant·es s'affiche ; les participant·es y sont classé selon le nombre de points obtenus (une réponse correcte vaut un point).

![](text/fr/abcd/example9.png){.center}

## Remarques sur les autres fonctions

Il est possible de modifier un quiz en cliquant sur **l'icône Modifier**.

![](text/fr/abcd/example10.png){.center}

**Un autre symbole de fonction est le test individuel.**

Cette îcone permet de passer en revue la collection de questions à titre individuel : Pour les personnes restées à la maison, etc. Elle permet d'afficher toutes les questions et on peut y répondre tout seul·e.

![](text/fr/abcd/example11.png){.center}

Pour faire cela, ce lien est transmis.

![](text/fr/abcd/example12.png){.center}

**Une autre icône est la duplication.**

Cette fonction permet de créer une copie de la collection de questions.

![](text/fr/abcd/example13.png){.center}

Pour cela, le nouveau nom de la collection est alors nécessaire.

![](text/fr/abcd/example14.png){.center}


**Un autre icône est de l'exportation.**

Cette fonction permet de sauvegarder la collection de questions dans son propre système.
Elle est le pendant de la fonction **Importer**, qui permet d'activer une telle collection de questions dans sa propre instance d'outil d'apprentissage.

![](text/de/abcd/example15.png){.center}


![](text/de/abcd/example16.png){.center}

Die Importfunktion braucht dann die Angabe der Datei, die importiert werden soll.


![](text/de/abcd/example17.png){.center}


**Das letzte Funktionssymbol ist der Papierkorb.**

Diese Funktion ermöglicht, die Fragesammlung im eigenen System zu löschen.

![](text/de/abcd/example18.png){.center}


Um irrtümliche Verluste zu vermeiden, gibt es eine Sicherheitsabfrage.


![](text/de/abcd/example19.png){.center}


