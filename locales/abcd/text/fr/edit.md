Si tu ne souhaites pas que le Quiz ABCD puisse être partagé par un lien, merci dez supprimer l’invitation.

Si tu souhates que le quiz ne soit visible que pour toi, coche **« privé »**.

Les questions peuvent être déplacées à l’aide des flèches.