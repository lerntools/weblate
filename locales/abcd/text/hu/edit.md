Ha nem szeretné, hogy az ABCD kvíz megosztásra kerüljön egy linken keresztül, kérjük, törölje a jegyet.

Ha a kvíz csak az Ön számára lesz elérhető, kérjük, jelölje meg "privátnak".

A kérdések a függőleges nyilak segítségével mozgathatók.