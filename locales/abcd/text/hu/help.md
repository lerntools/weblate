# ABCD kvíz

## Útmutató a szerzőknek


Az "ABCD kvíz" a "Ki lesz a milliomos?" című műsor közönségkérdésének és a "közlekedési lámpás kvíznek" a keveréke. A játék több kérdésből áll, amelyek mindegyikére négy válaszlehetőség van (A, B, C és D), amelyek közül csak egy helyes. A kérdést minden résztvevő egyszerre kapja meg (a gerendán), és az okostelefonjukon egy "titkos szavazás" során választják ki a választ. A kérdés megválaszolására rendelkezésre álló idő korlátozott. A moderátornak és a résztvevőknek nincs szükségük semmilyen speciális szoftverre, elegendő egy naprakész böngésző.



## Játék indítása

Csak a weboldalon regisztrált, megfelelő jogosultsággal rendelkező személy indíthat új játékot vagy hozhat létre új kérdéseket moderátorként:

1. Jelentkezzen be a weboldalra (fontos, különben az ABCD kvíz megjelenik a résztvevők számára).
2. navigáljon az ABCD kvízelemhez

![](text/hu/abcd/example1.png){.center}

3. kattintson a szimbólumra az indításhoz (háromszög)

![](text/hu/abcd/example2.png){.center}

Minden saját készítésű kérdésgyűjtemény elérhető, valamint azok is, amelyeket más szerzők nyilvánosnak jelöltek meg. A keresés egyszerűsítése érdekében a kijelző a kiválasztási listában egy adott témára korlátozható.

Mások nyilvános gyűjteményei nem szerkeszthetők vagy törölhetők. Vagyis ezek a szimbólumok nem erre a célra jelennek meg.

![](text/hu/abcd/example3.png){.center}

## Lobbi

Az ABCD kvíz elindítása után először az előcsarnok jelenik meg, amely egy QR-kódot tartalmaz a résztvevők számára.
A QR-kód helyett a megjelenített URL és a kulcs is használható.

![](text/hu/abcd/example4.png){.center}


Az elején minden résztvevőt megkérnek, hogy adjon meg egy NickNevet ( legalább 3 karakter hosszúságú legyen, s ez nem kerül véglegesen tárolásra az adatbázisban). Ez a becenév ezután megjelenik a lobby jobb oldali részén.

![](text/hu/abcd/example5.png){.center}

A nem megfelelő neveket a szemetes szimbólummal ki lehet tiltani a játékból. A résztvevők számára most egy egyszerű oldal jelenik meg négy gombbal (A, B, C, D).

![](text/hu/abcd/example6.png){.center}

## Kérdések és válaszok

Ha minden résztvevő részt vesz a játékban, azaz látható a lobbyban, a moderátor a "Start" gomb megnyomásával elindítja a kvízt. A kérdések és a válaszok felváltva jelennek meg. A résztvevők csak akkor szavazhatnak, ha egy kérdés aktív.

![](text/hu/abcd/example7.png){.center}

## Tippek a működéshez

A görgetéshez a kék nyíl helyett a szóköz is használható.

Minden kérdés után a helyes válasz megfejtése következik. 

![](text/hu/abcd/example8.png){.center}

Ezután a résztvevők számára megjelenik az aktuális közbenső pontszám.
A játék végén megjelenik a résztvevők áttekintése, az elért pontok száma szerint rendezve (egy helyes válasz egy pontnak számít).

![](text/hu/abcd/example9.png){.center}

## Megjegyzések a többi funkcióhoz

A kvíz megváltoztatására a **Szerkesztés** funkció ikonjával van lehetőség.

![](text/hu/abcd/example10.png){.center}

**Egy másik funkció ikonja az egyszeri teszt.**

Ez a funkció lehetővé teszi, hogy egyetlen személyként menjen végig a kérdéssorozaton: Azoknak, akik otthon maradtak stb. Ezután egy pillantással láthatja az összes kérdést, és elvégezheti azokat magának.

![](text/hu/abcd/example11.png){.center}

Ezt a linket ezután továbbítjuk.

![](text/hu/abcd/example12.png){.center}

**Egy másik funkció ikonja a Duplikálás.**

Ez a Import funkció megfelelője, amely lehetővé teszi egy ilyen kérdésgyűjtemény aktiválását a saját tanulási eszközpéldányban.

![](text/hu/abcd/example13.png){.center}

Ezután a gyűjtemény új neve szükséges.

![](text/hu/abcd/example14.png){.center}


**Egy másik funkció ikonja az Exportálás.**

Ez a funkció lehetővé teszi a kérdésgyűjtemény elmentését a saját rendszerben.
Ez a **Import funkció** megfelelője, amely lehetővé teszi egy ilyen kérdésgyűjtemény aktiválását a saját tanulási eszközpéldányban.

![](text/hu/abcd/example15.png){.center}


![](text/hu/abcd/example16.png){.center}

Az importáló funkciónak ezután szüksége van az importálandó fájl megadására.


![](text/hu/abcd/example17.png){.center}


**Az utolsó funkció ikonja a szemetes.**

Ez a funkció lehetővé teszi a kérdésgyűjtemény törlését a saját rendszerében.

![](text/hu/abcd/example18.png){.center}


A téves veszteségek elkerülése érdekében van egy biztonsági lekérdezés.


![](text/hu/abcd/example19.png){.center}


