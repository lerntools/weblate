Hvis du ikke ønkser at ABCD-spørrekonkurransen skal deles via lenke, kan du slette billettnumeret.

Hvis du vil at spørrekonkurransen kun skal være synlig for deg, kan du markere den som «Privat».

Spørsmål kan flyttes opp og ned med piltastene.