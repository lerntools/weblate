# ABCD-spørrekonkurranse

## Instruks for forfattere


«ABCD»-spørrekonkurransen er en variant av publikumsspørsmålet på «Hvem vil bli millionær?». Et spill består av flere spørsmål, hvor hver av dem har fire svaralternativer (A, B, C, og D), der kun ett er riktig. Spørsmålet vises for deltagerne samtidig (på projektoren) og de velger svaret i en «hemmelig avstemming» på smarttelefonene sine. Tiden avsatt til besvarelse av spørsmålet er begrenset. Moderatoren og deltagerne trenger ikke spesialprogramvare. En moderne nettleser holder.



## Oppstart av spill

Kun dem registrert på nettsiden med tilstrekkelige rettigheter (som administratorer) kan starte nye spill eller opprette nye spørsmål:

1. Logg inn på nettsiden (viktig, for ellers vil ABCD-spørrekonkurransen vises i deltagernes visning)
2. Gå til elementet «ABCD-spørrekonkurranse»

![](text/nb/abcd/example1.png){.center}

3. Klikk på symbolet for å starte (triangel)

![](text/nb/abcd/example2.png){.center}

Alle selvopprettede spørsmål er tilgjengelige, sågar også dem markert som offentlige av andre forfattere. For å forenkle søket, kan visningen begrenses til et gitt emne i utvalgslisten.

Offentlige samlinger tilhørende andre kan ikke redigeres og ikke slettes. Altså vises ikke ikonene for dette.

![](text/nb/abcd/example3.png){.center}

## Lobbyen

Etter at du starter spørrekonkurransen vises lobbyen. Den viste QR-koden kan brukes av deltagere for å ta del i den.
Alternativt kan nettadressen for lobbyen eller den viste nøkkelen brukes.

![](text/nb/abcd/example4.png){.center}


I begynnelsen blir alle deltakerne bedt om å tildele et kallenavn (minst 3 tegn langt, som ikke er permanent lagret i databasen). Dette kallenavnet vises deretter i høyre område av lobbyen.

![](text/nb/abcd/example5.png){.center}

Deltagere med upassende kallenavn kan kastes ut av lobbyen med papirkurvssymbolet ved siden av respektive kallenavn. Deretter vises en side med fire knapper (A, B, C, D) for deltagerne.

![](text/nb/abcd/example6.png){.center}

## Spørsmål og svar

Hvis alle deltagerne er å finne i lobbyen og de har valgt et kallenavn kan verten (som også er moderator for spørrekonkurransen) starte spørrekonkurransen med «Start»-knappen. Dette setter igang spørsmålsrekken. Deltagere kan kun besvare aktive spørsmål.

![](text/nb/abcd/example7.png){.center}

## Brukstips

Istedenfor den blå pilen for rulling kan mellomromstasten også brukes.

Etter hvert spørsmål følger rett svar. 

![](text/nb/abcd/example8.png){.center}

Nåværende poengsum vises alltid for deltagerne.
På slutten av spilet vises en oversikt over deltagerne, sortert etter antall poeng oppnådd. (Ett rett svar teller som ett poeng).

![](text/nb/abcd/example9.png){.center}

## Hint for de andre funksjonene

Er det mulig å endre en spørrekonkurranse via **Rediger**-funksjonsikonet.

![](text/nb/abcd/example10.png){.center}

**Et annet funksjonsikon er «Eksport»**

Denne funksjonen lar deg gå gjennom spørsmålssettet alene: For dem som var hjemmeværende, osv. Da kan du utforske alle problemene i en oversikt og løse dem selv.

![](text/nb/abcd/example11.png){.center}

Denne lenken blir delt videre i dette henseende.

![](text/nb/abcd/example12.png){.center}

**Et annet funksjonsikon er duplisering.**

Denne funksjonen lar deg lage en kopi av spørsmålssamlingen.

![](text/nb/abcd/example13.png){.center}

Dette krever det nye navnet på samlingen.

![](text/nb/abcd/example14.png){.center}


**Et annet funksjonsikon er «Eksport»**

Denne funksjonen gjør det mulig å lagre spørsmålssamlingen i ens eget system.
Dette er motsatsen til «Import»-funksjonen, som lar deg aktivere en slik spørsmålssamling i ens egen læringsverktøyinstans.

![](text/nb/abcd/example15.png){.center}


![](text/nb/abcd/example16.png){.center}

«Import»-funksjonen trenger så navnet på filen som skal importeres.


![](text/nb/abcd/example17.png){.center}


**Det siste funksjonsikonet er papirkurven**

Denne funksjonen lar deg slette spørsmålssamlingen i ditt eget system.

![](text/nb/abcd/example18.png){.center}


For å unngå feilaktige tap er det et sikkerhetsspørsmål.


![](text/nb/abcd/example19.png){.center}


