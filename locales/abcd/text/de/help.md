# ABCD-Quiz

## Anleitung für Autor:innen


Das „ABCD-Quiz“ ist eine Mischung aus der Publikumsfrage bei „Wer wird Millionär?“ und dem „Ampelquiz“. Ein Spiel besteht aus mehreren Fragen, zu denen es jeweils vier vorgegebene Antworten (A, B, C und D) gibt, von denen nur eine zutifft. Die Frage wird dabei gleichzeitig allen Teilnehmenden präsentiert (am Beamer), wobei diese die Antwort in einer „geheimen Wahl“ über ihr Smartphone treffen. Der Zeitraum zum Beantworten einer Frage ist dabei eingeschränkt. Moderator:in und Teilnehmende benötigen keine spezielle Software, ein aktueller Browser ist ausreichend.



## Starten eines Spiels

Nur eine auf der Webseite registrierte Person mit der entsprechenden Berechtigung, kann als Moderator:in ein neues Spiel starten oder auch neue Fragen anlegen:

1. Anmelden an der Webseite (wichtig, sonst erscheint das ABCD-Quiz in der Teilnehmenden-Sicht)
2. Navigation zum Punkt ABCD-Quiz

![](text/de/abcd/example1.png){.center}

3. Anklicken des Symbols zum Starten (Dreieck)

![](text/de/abcd/example2.png){.center}

Zur Verfügung stehen alle selbst erstellten Fragensammlungen sowie solche, die von anderen Autor:innen als öffentlich gekennzeichnet wurden. Um die Suche zur vereinfachen, kann in der Auswahlliste die Anzeige auf ein bestimmtes Thema / Fach beschränkt werden.

Öffentliche Sammlungen von anderen können nicht bearbeitet und nicht gelöscht werden. D.h. diese Symbole werden dazu nicht angezeigt.

![](text/de/abcd/example3.png){.center}

## Die Lobby

Nach dem Starten des ABCD-Quiz erscheint zunächst die Lobby, die einen QR-Code für die Teilnehmenden enthält.
Anstelle des QR-Codes können auch die angezeigte URL sowie der Schlüssel verwendet werden.

![](text/de/abcd/example4.png){.center}


Zu Beginn werden alle Teilnehmenden aufgefordert, einen NickName zu vergeben (mindestens 3 Zeichen lang, der nicht permanent in der Datenbank gespeichert wird). Dieser NickName erscheint anschließend im rechten Bereich der Lobby.

![](text/de/abcd/example5.png){.center}

Unpassende Namen können über das Mülleimersymbol aus dem Spiel verbannt werden. Bei den Teilnehmenden wird nun eine schlichte Seite mit vier Buttons (A, B, C, D) angezeigt.

![](text/de/abcd/example6.png){.center}

## Fragen & Antworten

Sind alle Teilnehmenden im Spiel, also in der Lobby sichtbar, beginnt die moderierende Person das Quiz mittels der Schaltfläche „Start“. Es erscheinen im Wechsel nun Fragen und Antworten. Die Teilnehmenden können dabei ihre Stimme nur abgeben, wenn eine Frage aktiv ist.

![](text/de/abcd/example7.png){.center}

## Tipps zur Bedienung

Anstelle des blauen Pfeils zum Weiterblättern kann auch die Leertaste genutzt werden.

Nach jeder Frage wird die korrekte Antwort aufgelöst. 

![](text/de/abcd/example8.png){.center}

Es wird dann der aktuelle Zwischenstand für die Teilnehmenden angezeigt.
Am Ende des Spiels erscheint eine Übersicht der Teilnehmenden, geordnet nach der erzielten Punktzahl (eine richtige Antwort zählt als ein Punkt).

![](text/de/abcd/example9.png){.center}

## Hinweise zu den weiteren Funktionen

Über das **Funktionssymbol Bearbeiten** ist es möglich, ein Quiz zu ändern.

![](text/de/abcd/example10.png){.center}

**Ein weiteres Funktionssymbol ist der Einzeltest.**

Diese Funktion ermöglicht, die Fragesammlung als einzelne Person durchzugehen: Für Zuhause gebliebene, etc. Dann sieht man alle Fragen auf einen Blick und kann sie für sich machen.

![](text/de/abcd/example11.png){.center}

Dazu wird dann dieser Link weitergegeben.

![](text/de/abcd/example12.png){.center}

**Ein weiteres Funktionssymbol ist das Duplizieren.**

Diese Funktion ermöglicht, eine Kopie der Fragesammlung anzulegen.

![](text/de/abcd/example13.png){.center}

Dazu wird dann der neue Name für die Sammlung benötigt.

![](text/de/abcd/example14.png){.center}


**Ein weiteres Funktionssymbol ist das Exportieren.**

Diese Funktion ermöglicht, die Fragesammlung im eigenen System zu speichern.
Sie ist das Gegenstück zur **Importfunktion**, die es ermöglicht, eine solche Fragesammlung in der eigenen Lerntoolinstanz zu aktivieren.

![](text/de/abcd/example15.png){.center}


![](text/de/abcd/example16.png){.center}

Die Importfunktion braucht dann die Angabe der Datei, die importiert werden soll.


![](text/de/abcd/example17.png){.center}


**Das letzte Funktionssymbol ist der Papierkorb.**

Diese Funktion ermöglicht, die Fragesammlung im eigenen System zu löschen.

![](text/de/abcd/example18.png){.center}


Um irrtümliche Verluste zu vermeiden, gibt es eine Sicherheitsabfrage.


![](text/de/abcd/example19.png){.center}


