Falls Sie nicht möchten, dass das ABCD-Quiz über einen Link freigegeben werden kann, so löschen Sie bitte das Ticket.

Soll das Quiz nur Ihnen zur Verfügung steht, dann kennzeichnen Sie es bitte als **„privat“**.

Fragen können mittels der vertikalen Pfeile verschoben werden.