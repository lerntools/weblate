Se non si desidera che il quiz ABCD venga condiviso tramite un link, si prega di cancellare il biglietto.

Se il quiz deve essere disponibile solo per voi, contrassegnatelo come "privato".

Le domande possono essere spostate utilizzando le frecce verticali.